using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;

using CookComputing.XmlRpc;

public class GetGroupServer : MarshalByRefObject, IGroup
{
    public string GetGroup(string groupName)
    {
        return Group.GetInstance(groupName).EncodeToJson();
    }
}

class _
{
    static void Main(string[] args)
    {
        IDictionary props = new Hashtable();
        props["name"] = "GroupHttpChannel";
        props["port"] = 5678;
        HttpChannel channel = new HttpChannel(
           props,
           null,
           new XmlRpcServerFormatterSinkProvider()
        );
        ChannelServices.RegisterChannel(channel, false);

        RemotingConfiguration.RegisterWellKnownServiceType(
          typeof(GetGroupServer),
          "getgroup.rem",
          WellKnownObjectMode.Singleton);

        Console.WriteLine("Press any key to shutdown service...");
        Console.ReadLine();
    }
}


