using System;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;

using CookComputing.XmlRpc;

class _
{
    static void Main(string[] args)
    {

        HttpChannel chnl = new HttpChannel(null, new XmlRpcClientFormatterSinkProvider(), null);
        ChannelServices.RegisterChannel(chnl, false);

        IGroup svr = (IGroup)Activator.GetObject(typeof(IGroup), "http://localhost:5678/getgroup.rem");

        while (true)
        {
            Console.Write("Enter group name: ");
            string groupName = Console.ReadLine();
            if (groupName == "")
                break;
            try
            {
                string group = svr.GetGroup(groupName);
                Console.WriteLine("Group {0}\n", group);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error occured {1}", e);
            }
        }
    }
}
