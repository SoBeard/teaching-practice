using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using CookComputing.XmlRpc;

public interface IGroup
{
  [XmlRpcMethod("examples.GetGroup")]
  string GetGroup(string groupName); 
}

public class Group
{
    public Group() { }

    public Group(string name, string curator, string praepotor, ArrayList students)
    {
        this.name = name;
        this.curator = curator;
        this.praepotor = praepotor;
        this.students = students;
    }

    public string name;
    public string curator;
    public string praepotor;
    public ArrayList students;

    public static Group GetInstance(string groupName)
    {
        var students = new ArrayList
            {
                "Sergeev Sergei Segeevich",
                "Dmitriev Dmitri Dmitrievich",
                "Andreev Andrei Andreevich"
            };

        return new Group(
            groupName, "Ivanov Ivan Ivanovich",
            "Petrov Petr Petrovich", students
        );
    }

    public string EncodeToXml()
    {
        XmlSerializer xsSubmit = new XmlSerializer(typeof(Group));

        using (var sww = new StringWriter())
        {
            using (XmlWriter writer = XmlWriter.Create(sww))
            {
                xsSubmit.Serialize(writer, this);
                return sww.ToString();
            }
        }
    }

    public string EncodeToJson()
    {
        return JsonConvert.SerializeObject(this);
    }
}
