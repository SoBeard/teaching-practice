﻿using System;
using CookComputing.XmlRpc;

namespace XMLRPC_Simple_Client
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            IStateName proxy = XmlRpcProxyGen.Create<IStateName>();
            for (int i = 1; i < 51; ++i) {
                Console.WriteLine("Making request...");
                string stateName = proxy.GetStateName(i);
                Console.WriteLine("Response: " + stateName + "\n");
            }
        }
    }

    [XmlRpcUrl("http://www.cookcomputing.com/xmlrpcsamples/RPC2.ashx")]
    public interface IStateName : IXmlRpcProxy
    {
        [XmlRpcMethod("examples.getStateName")]
        string GetStateName(int stateNumber);
    }


}
