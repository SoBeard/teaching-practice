package com.example.macbook.deeplink;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView txtData = findViewById(R.id.txtData);

        String text = "---";

        if (getIntent() != null) {
            if (getIntent().getData() != null) {
                text = getIntent().getData().getQueryParameter("text");
            }
        }

        txtData.setText(text);
    }
}
