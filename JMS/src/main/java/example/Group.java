package example;

import java.util.ArrayList;
import java.util.List;

public class Group {

    private String name;
    private String curator;
    private String praepotor;
    private List<String> students;

    public Group() {
    }

    public Group(String name, String curator, String praepotor, List<String> students) {
        this.name = name;
        this.curator = curator;
        this.praepotor = praepotor;
        this.students = students;
    }

    public static Group getInstance() {
        List<String> students = new ArrayList<>();
        students.add("Sergeev Sergei Segeevich");
        students.add("Dmitriev Dmitri Dmitrievich");
        students.add("Andreev Andrei Andreevich");

        return new Group(
                "iu611", "Ivanov Ivan Ivanovich",
                "Petrov Petr Petrovich", students
        );
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCurator(String curator) {
        this.curator = curator;
    }

    public void setPraepotor(String praepotor) {
        this.praepotor = praepotor;
    }

    public void setStudents(List<String> students) {
        this.students = students;
    }

    public String getName() {
        return name;
    }

    public String getCurator() {
        return curator;
    }

    public String getPraepotor() {
        return praepotor;
    }

    public List<String> getStudents() {
        return students;
    }

    @Override
    public String toString() {
        return String.format(
                "Group %s\nCurator: %s\nPraepotor: %s\nStudents:\n%s",
                name, curator, praepotor, String.join("\n", students)
        );
    }
}
