package example;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Receiver {

    public static final String DESTINATION_CODE = "receiver";

    public Receiver() {
        System.out.println("Instantiating receiver...\n");
    }

    @JmsListener(destination = DESTINATION_CODE, containerFactory = "jmsContainerFactory")
    public void receiveMessage(Group group) {
        System.out.println("Message received!\n" + group);
    }

}
