﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace NAMEDCHANNELS
{
    class Client
    {
        static void Main(string[] args)
        {
            var client = new NamedPipeClientStream(GetPipeName());
            client.Connect();
            StreamReader reader = new StreamReader(client);
            StreamWriter writer = new StreamWriter(client);

            Console.WriteLine("Named channel created");
            Console.WriteLine("Write message to send or ENTER to finish");

            while (true)
            {
                string input = Console.ReadLine();
                if (String.IsNullOrEmpty(input)) break;
                writer.WriteLine(input);
                writer.Flush();
            }
        }

        static String GetPipeName()
        {
            return "CustomPipe";
        }
    }
}
