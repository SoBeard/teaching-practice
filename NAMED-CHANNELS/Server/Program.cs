﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Server
{
    class Server
    {
        public static void Main(string[] args)
        {
            var server = new NamedPipeServerStream(GetPipeName());
            server.WaitForConnection();
            StreamReader reader = new StreamReader(server);
            StreamWriter writer = new StreamWriter(server);

            Console.WriteLine("Named channel created");

            while (true)
            {
                Console.WriteLine("Message received: ");
                var line = reader.ReadLine();
                writer.WriteLine(line);
                writer.Flush();
                Console.Write(line);
            }

        }

        static String GetPipeName()
        {
            return "CustomPipe";
        }
    }
}
