package com.example.macbook.share

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.AppCompatButton
import android.content.Intent


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<AppCompatButton>(R.id.btnShare).setOnClickListener {
            shareText()
        }

    }

    private fun shareText() {
        val shareBody = getString(R.string.group_info)
        val sharingIntent = Intent(android.content.Intent.ACTION_SEND)

        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody)

        startActivity(Intent.createChooser(sharingIntent, getString(R.string.group_info_title)))
    }
}
