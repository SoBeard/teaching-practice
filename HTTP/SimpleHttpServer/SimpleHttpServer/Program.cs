﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;

namespace HTTPServer
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Running sync server.");
            new SyncServer();
        }
    }

    public class SyncServer
    {

        HttpListener listener;

        private readonly HashSet<string> allowedQueryParams;

        public SyncServer()
        {
            allowedQueryParams = new HashSet<string>();
            InitAllowedQueryParams();

            listener = new HttpListener();

            listener.Prefixes.Add("http://localhost:8081/");
            listener.Prefixes.Add("http://127.0.0.1:8081/");

            listener.Start();

            while (true)
            {
                HandleRequests();
            }
        }

        private void InitAllowedQueryParams()
        {
            if (allowedQueryParams == null)
            {
                throw new InvalidOperationException("Init allowedQueryParams first");
            }

            allowedQueryParams.Add("name");
        }

        private void HandleRequests()
        {
            try
            {
                Console.WriteLine("Waiting for request...");
                var context = listener.GetContext();

                Console.WriteLine("Request detected! Handling...");

                PrintReuquestInfo(context.Request);

                context.Response.StatusCode = 200;
                context.Response.SendChunked = true;

                if (context.Request.HttpMethod.ToLower().Equals("get"))
                {
                    HandleGet(context);
                }
                else
                {
                    context.Response.StatusCode = 405;
                    WriteString(context, "405 / Method not allowed");
                    context.Response.Close();
                    return;
                }

                Console.WriteLine("Handling successfull!");

                Console.WriteLine("Closing connection...\n");

                context.Response.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error occured: " + e.Message);
            }
        }

        private void PrintReuquestInfo(HttpListenerRequest request)
        {
            Console.WriteLine("Request: " + request.HttpMethod.ToString()
                              + " " + request.RawUrl.ToString());
        }

        private void WriteString(HttpListenerContext context, string value)
        {
            var bytes = Encoding.UTF8.GetBytes(value);
            context.Response.OutputStream.Write(bytes, 0, bytes.Length);
        }

        private void HandleGet(HttpListenerContext context)
        {
            if (context.Request.QueryString.Count == 0)
            {
                WriteString(context, "200 / OK");
                return;
            }

            ParseNames(context);
        }

        private void ParseNames(HttpListenerContext context)
        {
            List<String> names = new List<string>();
            for (int i = 0; i < context.Request.QueryString.Count; ++i)
            {
                if (allowedQueryParams.Contains(context.Request.QueryString.GetKey(i)))
                {
                    Console.WriteLine(context.Request.QueryString.GetKey(i) + ": " + context.Request.QueryString.Get(i));
                    names.Add(context.Request.QueryString.Get(i));
                }
            }

            if (names.Count > 0)
            {
                WriteString(context, String.Format("Hello {0}!", String.Join(", ", names)));
            } else {
                WriteString(context, String.Format("Hello unknown!"));
            }
        }
    }
}
