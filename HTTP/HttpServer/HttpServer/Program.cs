﻿// Идея этого примера – показать, работу чуть более сложного HTTP сервера,
// написанном в REST-стиле.
//
// В данном примере у студента должно сложиться представление о следующих вещах:
// - HTTP-методы;
// - URL-ы, по которым сервер понимает, что ему нужно сделать (классическая реализация REST-стиля);
// - Форматы передачи данных (здесь JSON и XML).
//
// 1. HTTP-методы
// Показываем, что в HTTP есть 9 разных методов, имеющих свое назначение.
// Самыми распространенными являютсяс GET и POST. Первый, как правило,
// применяется для получения каких-то данных. Второй – для передачи данных
// на сервер.
// Эту мысль иллюстрируем двумя серверными методами /group/info – получение
// информации о группе студентов, /group/save - якобы "сохранение" информации
// о группе в БД сервера (на самом деле просто считываем переданные данные 
// и отправляем обратно).
//
// 2. URL-ы
// Здесь показываем классическую реализацию REST-стиля через задание своего
// запроса URL-ом и, опционально, query-параметрами или body.
// В примере, приняв запрос, сервер ориентируется на метод запроса, смотрит,
// есть ли URL запроса в списке реализованных (allowedGetUrls или allowedPostUrls).
// Если есть, то выполняет соответствующие действия.
// Например, POST-запрос /group/save, очевидно, выполняет сохранение группы,
// переданной в body запроса.
//
// 3. Форматы передачи данных
// Здесь показываем, что данные теоретически могут передаваться в разным форматах.
// Для примера в метод /group/info/<id>?format=<json|xml> введен query-параметр
// format – формат передачи ответа от сервера клиенту.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace HTTPServer
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Running sync server.");
            new SyncServer();
        }
    }

    public class SyncServer
    {
        private const string Host = "http://127.0.0.1:8081/";
        private const string GetGroupInfoUrl = "/group/info/\\d*";
        private const string PostSaveGroupUrl = "/group/save";
        private const int GetGroupInfoCode = 0;
        private const int PostGroupSaveCode = 0;
        private const string JSONFormat = "json";
        private const string XMLFormat = "xml";

        HttpListener listener;

        private readonly Dictionary<string, int> allowedGetUrls;
        private readonly Dictionary<string, int> allowedPostUrls;

        private HttpServer.Group group;

        public SyncServer()
        {
            allowedGetUrls = new Dictionary<string, int>();
            InitAllowedGetUrls();
            allowedPostUrls = new Dictionary<string, int>();
            InitAllowedPostUrls();
            InitGroup();

            listener = new HttpListener();

            listener.Prefixes.Add(Host);

            listener.Start();

            while (true)
            {
                HandleRequests();
            }
        }

        private void InitAllowedGetUrls()
        {
            if (allowedGetUrls == null)
            {
                throw new InvalidOperationException("Init allowedGetUrls first");
            }

            allowedGetUrls.Add(GetGroupInfoUrl, GetGroupInfoCode);
        }

        private void InitAllowedPostUrls()
        {
            if (allowedPostUrls == null)
            {
                throw new InvalidOperationException("Init allowedPostUrls first");
            }

            allowedPostUrls.Add(PostSaveGroupUrl, PostGroupSaveCode);
        }

        private void InitGroup()
        {
            var students = new ArrayList
            {
                "Sergeev Sergei Segeevich",
                "Dmitriev Dmitri Dmitrievich",
                "Andreev Andrei Andreevich"
            };
            group = new HttpServer.Group(
                "iu611", "Ivanov Ivan Ivanovich",
                "Petrov Petr Petrovich", students
            );
        }

        private void HandleRequests()
        {
            try
            {
                Console.WriteLine("Waiting for request...");
                var context = listener.GetContext();

                Console.WriteLine("Request detected! Handling...");

                PrintReuquestInfo(context.Request);

                context.Response.StatusCode = 200;
                context.Response.SendChunked = true;

                if (context.Request.HttpMethod.ToLower().Equals("get"))
                {
                    HandleGet(context);
                }
                else if (context.Request.HttpMethod.ToLower().Equals("post"))
                {
                    HandlePost(context);
                }
                else
                {
                    context.Response.StatusCode = 405;
                    WriteString(context, "405 / Method not allowed");
                    context.Response.Close();
                    return;
                }

                Console.WriteLine("Handling successfull!");

                Console.WriteLine("Closing connection...\n");

                context.Response.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error occured: " + e.Message);
            }
        }

        private void PrintReuquestInfo(HttpListenerRequest request)
        {
            Console.WriteLine("Request: " + request.HttpMethod.ToString()
                              + " " + request.RawUrl.ToString());
        }

        private void WriteString(HttpListenerContext context, string value)
        {
            var bytes = Encoding.UTF8.GetBytes(value);
            context.Response.OutputStream.Write(bytes, 0, bytes.Length);
        }

        private void HandleGet(HttpListenerContext context)
        {
            var rawUrl = context.Request.RawUrl;
            switch (MatchUrl(rawUrl, allowedGetUrls))
            {
                case GetGroupInfoCode:
                    string[] formatArray = context.Request.QueryString.GetValues("format");
                    var format = formatArray.Length > 0 ? formatArray[0] : JSONFormat;

                    GetGroupById(
                        context, Int32.Parse(Regex.Match(rawUrl, "\\d+").Value), format
                    );
                    break;
                default:
                    context.Response.StatusCode = 404;
                    WriteString(context, "404 / Not Found");
                    break;
            }
        }

        private int MatchUrl(string rawUrl, Dictionary<string, int> allowedUrls)
        {
            Regex regex;

            foreach (string urlPattern in allowedUrls.Keys)
            {
                regex = new Regex(urlPattern);
                if (regex.Match(rawUrl).Success)
                {
                    return allowedUrls[urlPattern];
                }
            }

            return -1;
        }

        private void GetGroupById(HttpListenerContext context, int groupId, string format)
        {
            switch (format)
            {
                case JSONFormat:
                    context.Response.ContentType = "application/json";
                    WriteString(context, group.EncodeToJson());
                    break;
                case XMLFormat:
                    context.Response.ContentType = "application/xml";
                    WriteString(context, group.EncodeToXml());
                    break;
                default:
                    context.Response.StatusCode = 400;
                    WriteString(context, "400 / Bad request, unknown format");
                    break;
            }
        }

        private void HandlePost(HttpListenerContext context)
        {
            var rawUrl = context.Request.RawUrl;
            switch (MatchUrl(rawUrl, allowedPostUrls))
            {
                case PostGroupSaveCode:
                    SaveGroup(context);
                    break;
                default:
                    context.Response.StatusCode = 404;
                    WriteString(context, "404 / Not Found");
                    break;
            }
        }

        private void SaveGroup(HttpListenerContext context)
        {
            System.IO.Stream body = context.Request.InputStream;
            System.IO.StreamReader reader = new System.IO.StreamReader(
                body, context.Request.ContentEncoding
            );

            WriteString(context, reader.ReadToEnd());

            body.Close();
            reader.Close();
        }

    }
}
