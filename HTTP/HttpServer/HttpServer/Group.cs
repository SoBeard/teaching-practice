﻿using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace HttpServer
{
    public class Group
    {
        public Group() { }

        public Group(string name, string curator, string praepotor, ArrayList students)
        {
            this.name = name;
            this.curator = curator;
            this.praepotor = praepotor;
            this.students = students;
        }

        public string name;
        public string curator;
        public string praepotor;
        public ArrayList students;

        public string EncodeToXml()
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(Group));

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, this);
                    return sww.ToString();
                }
            }
        }

        public string EncodeToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
