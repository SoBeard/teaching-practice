﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace UdpReceiver
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Instantiating receiver...");
            new Receiver();
        }

        public class Receiver
        {
            public Receiver()
            {
                IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5556);

                UdpClient receivingUdpClient = new UdpClient(endPoint);

                try
                {
                    while (true)
                    {
                        byte[] rawBytes = receivingUdpClient.Receive(ref endPoint);

                        string rawString = Encoding.UTF8.GetString(rawBytes);
                        Console.WriteLine("Raw message: " + rawString + "\n\n");

                        Group group = Group.DeserializeFromJson(rawString);

                        Console.WriteLine(group);

                        byte[] bytes = Encoding.UTF8.GetBytes(group.EncodeToXml());
                        receivingUdpClient.Send(bytes, bytes.Length, endPoint);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e + "\n  " + e.Message);
                }
            }

        }
    }
}
