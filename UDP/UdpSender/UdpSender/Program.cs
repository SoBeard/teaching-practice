﻿using System;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.Text;

namespace UdpSender
{
    class MainClass
    {


        public static void Main(string[] args)
        {
            Console.WriteLine("Instantiating Sender...");
            new Sender();
        }

        public class Sender
        {
            private const string RemoteIPAddress = "127.0.0.1";

            private static IPAddress remoteIPAddress;
            private static int remotePort = 5556;

            private UdpClient sender;
            IPEndPoint endPoint;
            public Sender()
            {
                try
                {
                    remoteIPAddress = IPAddress.Parse(RemoteIPAddress);

                    sender = new UdpClient();

                    endPoint = new IPEndPoint(remoteIPAddress, remotePort);

                    Console.WriteLine("Press something to send messages...");
                    Console.ReadLine();

                    SendDatagram(Group.GetInstance().EncodeToJson());

                    while (true)
                    {
                        byte[] rawBytes = sender.Receive(ref endPoint);

                        string rawString = Encoding.UTF8.GetString(rawBytes);
                        Console.WriteLine("Raw message: " + rawString + "\n\n");

                        Group group = Group.DeserializeFromXml(rawString);

                        Console.WriteLine(group);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString() + "\n  " + e.Message);
                }
            }

            private void SendDatagram(string datagram)
            {
                byte[] bytes = Encoding.UTF8.GetBytes(datagram);
                sender.Send(bytes, bytes.Length, endPoint);
            }
        }
    }
}
