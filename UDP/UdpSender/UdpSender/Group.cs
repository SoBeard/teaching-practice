﻿using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace UdpSender
{
    public class Group
    {
        public Group() { }

        public Group(string name, string curator, string praepotor, ArrayList students)
        {
            this.name = name;
            this.curator = curator;
            this.praepotor = praepotor;
            this.students = students;
        }

        public string name;
        public string curator;
        public string praepotor;
        public ArrayList students;

        public static Group GetInstance()
        {
            var students = new ArrayList
            {
                "Sergeev Sergei Segeevich",
                "Dmitriev Dmitri Dmitrievich",
                "Andreev Andrei Andreevich"
            };

            return new Group(
                "iu611", "Ivanov Ivan Ivanovich",
                "Petrov Petr Petrovich", students
            );
        }

        public override string ToString()
        {
            string result = "";

            string[] fieldsArray = { name, curator, praepotor };
            result += String.Join("\n", fieldsArray) + "\n";
            result += String.Join("\n", students.ToArray());

            return result;
        }

        public string EncodeToXml()
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(Group));

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, this);
                    return sww.ToString();
                }
            }
        }

        public static Group DeserializeFromXml(string xmlString)
        {
            var serializer = new XmlSerializer(typeof(Group));
            using (TextReader reader = new StringReader(xmlString))
            {
                return (Group)serializer.Deserialize(reader);
            }
        }

        public string EncodeToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static Group DeserializeFromJson(string jsonString)
        {
            return JsonConvert.DeserializeObject<Group>(jsonString);
        }
    }
}
