﻿using System;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;

namespace Server
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            TcpListener server = null;
            try
            {
                Int32 port = 13000;
                IPAddress host = IPAddress.Parse("127.0.0.1");

                server = new TcpListener(host, port);

                server.Start();

                Byte[] bytes = new Byte[256];
                String data = null;

                while (true)
                {

                    TcpClient client = server.AcceptTcpClient();

                    data = null;

                    NetworkStream stream = client.GetStream();

                    int i;

                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                        Console.WriteLine("Received: {0}", data);
                    }

                    client.Close();
                }
            }

            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }

            finally
            {
                server.Stop();
            }

            Console.Read();
        }
    }
}
