﻿using System;
using System.Net.Sockets;

namespace Client
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            try
            {
                Int32 port = 13000;
                string host = "127.0.0.1";
                string message = "IU6-23M";

                TcpClient client = new TcpClient(host, port);

                Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);

                NetworkStream stream = client.GetStream();

                stream.Write(data, 0, data.Length);

                Console.WriteLine("Sent: {0}", message);

                stream.Close();
                client.Close();
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("ArgumentNullException: {0}", e);
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }

            Console.WriteLine("\n Press Enter to continue...");
            Console.Read();
        }
    }
}
